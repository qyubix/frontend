import { Component, OnInit } from '@angular/core';
import { environment } from "../../../environments/environment"
import {ProductService} from "../../service/product.service";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})

export class HomeComponent implements OnInit {
  server= environment.dataserve;
  computers: any[];
  beauties: any[];
  books: any[];
  clothings: any[];
  shoes: any[];
  electronics: any[];
  games: any[];
  gardens: any[];
  healths: any[];
  homes: any[];
  jeweleries: any[];
  kids: any[];
  movies: any[];
  outdoors: any[];
  sports: any[];

  constructor (private productService: ProductService) {}

  ngOnInit() {
    this.getHomes();
  }

  getHomes() {
    this.productService.getProductByCategory("computer",12)
        .subscribe(computers => this.computers = computers.content);

    this.productService.getProductByCategory("beauty",12)
        .subscribe(beauties => this.beauties = beauties.content);

    this.productService.getProductByCategory("book",12)
        .subscribe(books => this.books = books.content);

    this.productService.getProductByCategory("clothing",12)
        .subscribe(clothings => this.clothings = clothings.content);

    this.productService.getProductByCategory("garden",12)
        .subscribe(gardens => this.gardens = gardens.content);

    this.productService.getProductByCategory("electronic",12)
        .subscribe(electronics => this.electronics = electronics.content);

    this.productService.getProductByCategory("game",12)
        .subscribe(games => this.games = games.content);

    this.productService.getProductByCategory("computer",12)
        .subscribe(healths => this.healths = healths.content);

    this.productService.getProductByCategory("home",12)
        .subscribe(homes => this.homes = homes.content);

    this.productService.getProductByCategory("jewelery",12)
        .subscribe(jeweleries => this.jeweleries = jeweleries.content);

    this.productService.getProductByCategory("kid",12)
        .subscribe(kids => this.kids = kids.content);

    this.productService.getProductByCategory("movie",12)
        .subscribe(movies => this.movies = movies.content);

    this.productService.getProductByCategory("outdoor",12)
        .subscribe(outdoors => this.outdoors = outdoors.content);

    this.productService.getProductByCategory("sport",12)
        .subscribe(sports => this.sports = sports.content);

    this.productService.getProductByCategory("shoe",12)
        .subscribe(shoes => this.shoes = shoes.content);
  }


}
