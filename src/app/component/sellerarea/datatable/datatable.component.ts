import { Component, OnInit } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {TransactionService} from "../../../service/transaction.service";

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements OnInit {
  server = environment.dataserve;
  totalpricesuccess:number=0;
  totalpricefail:number=0;
  totalpriceonprogress:number=0;
  totalpricebooked:number=0;
  transactionssuccess: any[];
  transactionfails: any[];
  transactiononprogress: any[];
  transactionbookeds: any[];
  printvisibility="visible";


  constructor(
    private transactionService: TransactionService
  ) { }


  ngOnInit() {
    this.transactionService
      .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(sessionStorage.getItem("key"),"success",1000)
      .subscribe(transactions => {
        this.transactionssuccess = transactions.content;
        this.totalpricesuccess=this.gettotalprice(this.transactionssuccess);
        });

    this.transactionService
      .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(sessionStorage.getItem("key"),"fail",1000)
      .subscribe(transactions => {
        this.transactionfails = transactions.content;
        this.totalpricefail=this.gettotalprice(this.transactionfails);
      });

    this.transactionService
      .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(sessionStorage.getItem("key"),"onprogress",1000)
      .subscribe(transactions => {
        this.transactiononprogress = transactions.content;
        this.totalpriceonprogress=this.gettotalprice(this.transactiononprogress);
      });

    this.transactionService
      .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus(sessionStorage.getItem("key"),"booked",1000)
      .subscribe(transactions => {
        this.transactionbookeds = transactions.content;
        this.totalpricebooked=this.gettotalprice(this.transactionbookeds);
      });
  }

   gettotalprice(transactions){
    let i:number;
    let total:number=0;

    for(i=0; i<transactions.length;i++){
      total +=transactions[i].product_discount_after * transactions[i].transaction_quantity;
    }
    return total;
  }
}
