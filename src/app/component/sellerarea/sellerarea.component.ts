import { Component, OnInit } from '@angular/core';
import {environment} from "../../../environments/environment";
import {UserService} from "../../service/user.service";
import {ProductService} from "../../service/product.service";
import {LoginService} from "../../service/login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'sellerarea',
  templateUrl: './sellerarea.component.html',
  styleUrls: ['./sellerarea.component.css']
})
export class SellerareaComponent implements OnInit {

  constructor() {}

  ngOnInit() {}
}
