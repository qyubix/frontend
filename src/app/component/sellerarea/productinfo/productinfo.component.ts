import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../service/user.service";
import {ChartData} from "./chartdata";
import {TransactionService} from "../../../service/transaction.service";
import {ChartDataBar} from "./chartdatabar";



@Component({
  selector: 'app-productinfo',
  templateUrl: './productinfo.component.html',
  styleUrls: ['./productinfo.component.css']
})
export class ProductinfoComponent implements OnInit {
  //for birth vs sign up line chart============================================================================
  birthVsSignUp: any[];
  birthVsSignUpUser = new Array();
  birthVsSignUpActivefrom = new Array();
  birthVsSignUpUserbirth= new Array();
  public birthVsSignUpLegend:boolean = true;
  public birthVsSignUpOption:any = this.chartData.getChartOptions1();
  public birthVsSignUpData:Array<any> = [
    {data: this.birthVsSignUpUserbirth,
      label: 'User Birth',
      backgroundColor: 'rgba(0, 0, 0, 0.0)',
      borderColor: '#a0ff98',
      pointBackgroundColor: '#0dff09',
      pointBorderColor: '#000000',
      pointHoverBorderColor: 'rgba(77,83,96,1)',
    },
    {data: this.birthVsSignUpActivefrom,
      label: 'Active From',
      backgroundColor: 'rgba(0, 0, 0, 0.0)',
      borderColor: '#adb4ff',
      pointBackgroundColor: '#1f33ff',
      pointBorderColor: '#000000',
      pointHoverBorderColor: 'rgba(77,83,96,1)'}
  ];

  parsearray(){
    for (var i = 0; i < this.birthVsSignUp.length; i++) {
      this.birthVsSignUpUser.push("\'"+this.birthVsSignUp[i].user_real_name+"\'");
      this.birthVsSignUpActivefrom.push(new Date(this.birthVsSignUp[i].user_active_from).getFullYear());
      this.birthVsSignUpUserbirth.push(new Date(this.birthVsSignUp[i].user_birth).getFullYear());
    }
    // console.log("new numbers is : " + this.birthVsSignUpUser );
    // console.log("new numbers is : " + this.birthVsSignUpActivefrom);
  }

  //for transaction vs status doughnut chart====================================================================
  public transactionVsStatusLabel:string[] = ['Booked', 'Success', 'On Progress','fail'];
  transactionBooked:number;
  transactionOnProgress:number;
  transactionSuccess:number;
  transactionfail:number;
  transactionCounter=0;

  public transactionVsStatus:number[]=new Array();
  private ChartColours: any[] =  [
    {
      backgroundColor: [
        "#FF6384",
        "#36A2EB",
        "#5cff3b",
        "#FFCE56"
      ],
      hoverBackgroundColor: [
        "#FF6384",
        "#36A2EB",
        "#5cff3b",
        "#FFCE56"
      ]
    }];

  pushTransactionArr(){
    if(this.transactionCounter==4){
      this.transactionVsStatus.push(this.transactionBooked);
      this.transactionVsStatus.push(this.transactionSuccess);
      this.transactionVsStatus.push(this.transactionOnProgress);
      this.transactionVsStatus.push(this.transactionfail);
    }
  }

  //for transaction vs year bar chart===========================================================================
  barDiscountGiver:any[];
  barUserName = new Array();
  totalDiscount = new Array();
  totalPrice = new Array();
  totalAfterDiscount = new Array();
  public transactionVsYearOption:any = this.chartDataBar.getChartOptions1();
  public barChartData:any[] = [
    {data:this.totalPrice, label: 'Total Price'},
    {data:this.totalAfterDiscount, label: 'Total After Discount'},
    {data:this.totalDiscount, label: 'Total Discount'}
  ];
  public barChartLabels:string[] = this.barUserName;
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;


  pushDiscountGiver(){
    for (var i = 0; i < this.barDiscountGiver.length; i++) {
      this.barUserName.push(this.barDiscountGiver[i].user_real_name);
      this.totalPrice.push(this.barDiscountGiver[i].total_price);
      this.totalAfterDiscount.push(this.barDiscountGiver[i].total_after_discount);
      this.totalDiscount.push(this.barDiscountGiver[i].total_discount);
    }
    console.log("new numbers is : " + this.birthVsSignUpUser );
    console.log("new numbers is : " + this.birthVsSignUpActivefrom);
  }

  constructor(private userService: UserService,
              private chartData:ChartData,
              private chartDataBar:ChartDataBar,
              private transactionService:TransactionService) {}

  ngOnInit() {
    this.userService.getAllUser(20)
      .subscribe(users => {this.birthVsSignUp = users.content,
          this.parsearray();});

    this.transactionService
      .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatusWithPagination
        (sessionStorage.getItem("key"),"booked",1000,0,1000)
      .subscribe(transactionBooked => {this.transactionBooked = transactionBooked.totalelements;
        this.transactionCounter++;
        this.pushTransactionArr();
        });

    this.transactionService
      .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatusWithPagination
        (sessionStorage.getItem("key"),"success",1000,0,1000)
      .subscribe(transactionSuccess => {
          this.transactionSuccess = transactionSuccess.totalelements;
          this.transactionCounter++;
          this.pushTransactionArr();
          });

    this.transactionService
      .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatusWithPagination
        (sessionStorage.getItem("key"),"onprogress",1000,0,1000)
      .subscribe(transactionOnProgress => {
          this.transactionOnProgress = transactionOnProgress.totalelements;
          this.transactionCounter++;
          this.pushTransactionArr();
          });

    this.transactionService
      .getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatusWithPagination
      (sessionStorage.getItem("key"),"fail",1000,0,1000)
      .subscribe(transactionOnProgress => {this.transactionfail = transactionOnProgress.totalelements;
        this.transactionCounter++;
        this.pushTransactionArr();
      });

    this.transactionService
      .getValuableUser(10)
      .subscribe(barDiscountGiver => {
        this.barDiscountGiver = barDiscountGiver.content;
        this.pushDiscountGiver();
      });
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }


}
