export class ChartData {
  public ChartOptions1:any = {
    responsive: true,
    drawborder:true,
    xAxes: [{
      type: 'time',
      time: {
        displayFormats: {
          year: 'YY',
        }
      }
    }],
    scales: {
      xAxes: [{
        ticks: {
          autoSkip: false,
          callback: function (value, index, values) {
            return 'User  ' + value;
          }
        }
      }]
    },
    tooltips: {
      mode: 'index',
      callbacks: {
        beforeFooter:function(tooltipItems, data) {
          var sum = 0;
          tooltipItems.forEach(function(tooltipItem) {
            sum = data.datasets[1].data[tooltipItem.index]-data.datasets[0].data[tooltipItem.index];
          });
          return 'Pertama kali menggunakan Qyubix saat umur : ' + sum+' tahun';
        },
        footer: function(tooltipItems, data) {
          var sum = 0;
          tooltipItems.forEach(function(tooltipItem) {
            sum = new Date().getFullYear() - data.datasets[1].data[tooltipItem.index];
          });
          return 'Sudah menggunakan Qyubix selama : '+sum+' tahun';
        },
      },
      footerFontStyle: 'normal'
    },
  };

  getChartOptions1(){
    return this.ChartOptions1;
  }
}
