export class ChartDataBar {
  public ChartBar:any = {

    scales: {
      yAxes: [{
        ticks: {

          // Return an empty string to draw the tick line but hide the tick label
          // Return `null` or `undefined` to hide the tick line entirely
          userCallback:function thousand_separator(input) {
            			return "RP. "+parseFloat(input).toLocaleString("in-ID");
            	}
        }}]
    },
  };

  getChartOptions1(){
    return this.ChartBar;
  }
}
