import { Component, OnInit,Input } from '@angular/core';
import 'rxjs/Rx';
import {ProductService} from "../../service/product.service";

@Component({
  selector: 'searchpopup',
  templateUrl: './searchpopup.component.html',
  styleUrls: ['./searchpopup.component.css'],
})

export class SearchpopupComponent implements OnInit {
  loading:any;
  @Input() sendvisi;
  visibility:string="hidden";
  searchMenuShrink : string = '';
  @Input() term:string;

  refresh(){
    window.location.reload();
  }
  searchmenu(){
    this.visibility= "visible";
  }

  searchmenuhide(){
    this.visibility= "hidden";
  }

  ngOnInit() {
    this.visibility =this.sendvisi;
    this.getHomes();
    this.loadersearch();
  }

  onScrolled(yPos : number) {
    this.searchMenuShrink = yPos ? "searchmenu-shrink" : "";
  }

  errorMessage: string;
  services: any[];
  constructor (private productService: ProductService) {
  }

  getHomes() {
    this.productService.getProductSearch(3000)
        .subscribe(
            services => this.services = services.content,
            error =>  this.errorMessage = <any>error);
  }

  loadersearch(){
  }
}
