import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { environment } from "../../../environments/environment"
import {Observable} from "rxjs";
import {DetailprodService} from "../../service/detailprod.service";

@Component({
  selector: 'app-detailprod',
  templateUrl: './detailprod.component.html',
  styleUrls: ['./detailprod.component.css'],
})
export class DetailprodComponent implements OnInit {
  errorMessage: string;
  server= environment.dataserve;
  id:string;
  quantity:number=1;
  store_id:string;
  product_name:string;
  product_description:string;
  product_price:string;
  product_discount:string;
  product_discount_after:string;
  product_rating:number;
  product_sold:string;
  product_image_total:number;
  image_number:number=1;
  private sub: any;
  starspath:string;
  items:any;
  searchValue:string;
  visibility:string='visible';


  constructor(private detailprodService:DetailprodService,private route: ActivatedRoute) {
    window.scrollTo(0,0);
  }

  ngOnInit() {
     this.route.params.subscribe(params => {let   id = params['id'];this.id = id;});

    this.detailprodService.getDetail(this.id)
         .subscribe(details => {
           this.store_id = details.product_store_key;
           this.product_name = details.product_name;
           this.product_description = details.product_description;
           this.product_price = details.product_price;
           this.product_discount = details.product_discount;
           this.product_discount_after = details.product_discount_after;
           this.product_price = details.product_price;
           this.product_sold = details.product_sold;
           this.product_rating = details.product_rating;
           this.product_image_total = details.product_image_total;
           },
            error => this.errorMessage = <any>error);
  }

  searchCalled(message:string):void{
    this.searchValue=message;
  }

  getstars(x){
    if (x == 1){
      return "imagemanagement/1stars.png";
    }else if (x == 2){
      return "imagemanagement/2stars.png";
    }else if (x == 3){
      return "imagemanagement/3stars.png";
    }else if (x <= 8){
      return "imagemanagement/4stars.png";
    }else if (x <= 10){
      return "imagemanagement/5stars.png";
    }
  }

  setProductImageTotal(x){
    this.product_image_total= x;
  }

  createRange(){
    this.items = [];
    for (var i = 1; i <= this.product_image_total; i++) {
        this.items.push(i);
      }
    return this.items;
  }

  changeimage(x){
    this.image_number=x;
  }

  buy(){
    this.detailprodService.buy( this.quantity,this.id);
  }



  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }


}
