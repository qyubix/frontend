import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { environment } from "../../../environments/environment"
import {ProductService} from "../../service/product.service";

@Component({
  selector: 'seemore',
  templateUrl: './seemore.component.html',
  styleUrls: ['./seemore.component.css'],
})

export class SeemoreComponent implements OnInit {
  server=environment.dataserve;
  id:string;
  idCat:string;
  private page=0;
  pageNumber:number;
  pageSize:number;
  totalElement:number;
  totalpages:number;
  categories:any[];
  searchValue:string;


  constructor(private productService:ProductService,private router: Router,private route: ActivatedRoute) {
    this.router.events.pairwise().subscribe((e) => {this.refresh()});
    this.id = '?';
  }

  ngOnInit() {
    this.route.params.subscribe(params => {let   id = params['id'];this.id = id;});
    this.getSeeMore();
  }

  searchCalled(message:string):void{
    this.searchValue=message;
  }

  setResponse(category:string,title:string){
    this.idCat = title;
    this.productService.getProductByCategoryWithPagination(category,60,this.page,1000)
      .subscribe(categories => {
        this.categories = categories.content;
        this.totalpages = categories.totalPages;
        this.pageSize = categories.size;
        this.pageNumber = categories.number;
        this.totalElement = categories.totalelements;
      });
  }

  getSeeMore(){
    if (this.id==='1') {
      this.setResponse("computer","COMPUTER PRODUCT");
    }else if (this.id==='2'){
      this.setResponse("beauty","BEAUTY PRODUCT");
    }else if (this.id==='3'){
      this.setResponse("book","BOOK PRODUCT");
    }else if (this.id==='4'){
      this.setResponse("clothing","CLOTHING PRODUCT");
    }else if (this.id==='5'){
      this.setResponse("shoe","SHOES PRODUCT");
    }else if (this.id==='6'){
      this.setResponse("electronic","ELEKTRONIK PRODUCT");
    }else if (this.id==='7'){
      this.setResponse("game","GAMES PRODUCT");
    }else if (this.id==='8'){
      this.setResponse("garden","GARDEN PRODUCT");
    }else if (this.id==='9'){
      this.setResponse("health","HEALTH PRODUCT");
    }else if (this.id==='10'){
      this.setResponse("home","HOME PRODUCT");
    }else if (this.id==='11'){
      this.setResponse("jewelery","JEWELERY PRODUCT");
    }else if (this.id==='12'){
      this.setResponse("kid","KIDS PRODUCT");
    }else if (this.id==='13'){
      this.setResponse("movie","MOVIES PRODUCT");
    }else if (this.id==='14') {
      this.setResponse("outdoor","OUTDOORS PRODUCT");
    }else if (this.id==='15') {
      this.setResponse("sport","SPORT PRODUCT");
    }
  }

  paginate(event){
    this.categories=null;
    this.page=event.page;
    this.getSeeMore();
  }

  refresh(){
    window.location.reload();
  }
}
