import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {
  searchValue:string;

  searchCalled(message:string):void{
    this.searchValue=message;
  }
  constructor() { }

  ngOnInit() {
  }

}
