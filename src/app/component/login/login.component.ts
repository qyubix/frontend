import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { LoginService } from '../../service/login.service';
import {LightboxModule} from 'primeng/primeng';
import {HeaderComponent} from "../header/header.component";


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              private http: Http,
              private loginService: LoginService,
              private headerComponent:HeaderComponent
  ) {}

  ngOnInit() {
  }

  login(username, password, role){
    this.loginService.login(username, password, role);
  }

  tutup(){
    this.headerComponent.setVisibleLoginPopup(false);
  }
}
