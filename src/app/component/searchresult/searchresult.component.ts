import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ActivatedRoute,Router } from '@angular/router';
import { environment } from "../../../environments/environment"
import {ProductService} from "../../service/product.service";

@Component({
  selector: 'searchresult',
  templateUrl: './searchresult.component.html',
  styleUrls: ['./searchresult.component.css']
})
export class SearchresultComponent implements OnInit {
  name:string;
  searches:any;
  server:string=environment.dataserve;
  pageNumber:number;
  pageSize:number;
  totalElement:number;
  totalpages:number;
  private page=0;
  searchValue:string;

  constructor(private route: ActivatedRoute, private router: Router,private productService:ProductService) {
    this.router.events.pairwise().subscribe((e) => {this.refresh()});
    this.route.params.subscribe(params => {
        let name = params['name'];this.name = name;
      });
      this.getSearch();
  }

  ngOnInit() {}

  searchCalled(message:string):void{
    this.searchValue=message;
  }

  getSearch() {
    this.productService.getProductByCategoryWithPagination(this.name,60,this.page,1000)
      .subscribe(searches => {
        this.searches = searches.content;
        this.totalpages = searches.totalPages;
        this.pageSize = searches.size;
        this.pageNumber = searches.number;
        this.totalElement = searches.totalelements;
      });
  }

  paginate(event){
    this.searches=null;
    this.page=event.page;
    this.getSearch();
  }

  refresh(){
    window.location.reload();
  }
}
