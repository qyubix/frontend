import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Router} from "@angular/router";
import {tokenNotExpired, JwtHelper} from "angular2-jwt";
import {ProductService} from "../../service/product.service";

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css','./headershrinked.component.css'],
})
export class HeaderComponent implements OnInit {
  search: any[];
  disableInput:boolean=true;
  btnLoginText:string="LOGIN";
  btnLoginColor="#ff030c";
  term:string;
  navbarShrink : string = '';
  navbarShrinkSecond : string = '';
  icoQyubixShrink : string = '';
  spaceShrink : string='';
  buttonBsShrink : string='';
  buttonAsShrink : string='';
  searchFieldShrink : string='';
  searchButtonShrink : string='';
  visibleLoginPopup = false;
  errorMessage:string;
  token =sessionStorage.getItem("token");
  jwtHelper: JwtHelper = new JwtHelper();

  constructor(
    private router:Router,
    private productService:ProductService
  ) {
    this.productService.getProductSearch(3000)
      .subscribe(search => {
          this.search = search;
          this.disableInput=false;
        },
        error => this.errorMessage = <any>error);
  }

  ngOnInit() {
    this.setLoginOrDashboard();
  }

  /* Called from    : this component
   * Function       : Detect scroll in the browser and give action (animate header)
   * Explanation    :When browser is scrolled, component with ngClass navbarShrink,navbarShrinkSecond,
   *                icoQyubixShrink etc will be add css with class navbar-shrink,navbar-shrink-second,
   *                ico-qyubix-shrink etc.
   *
   *                yPos? is the condition which will check Y possition, if this event triggered
   *                then this method will be called
   * */
  onScrolled(yPos : number) {
    if(yPos){
      this.navbarShrink ="navbar-shrink";
      this.navbarShrinkSecond ="navbar-shrink-second" ;
      this.icoQyubixShrink = "ico-qyubix-shrink";
      this.spaceShrink = "space-shrink";
      this.buttonBsShrink = "button-beforeShrink-shrink";
      this.buttonAsShrink = "button-afterShrink-shrink";
      this.searchFieldShrink = "search-field-shrink";
      this.searchButtonShrink = "search-button-shrink";
    }else {
      this.navbarShrink ="";
      this.navbarShrinkSecond ="" ;
      this.icoQyubixShrink = "";
      this.spaceShrink = "";
      this.buttonBsShrink = "";
      this.buttonAsShrink = "";
      this.searchFieldShrink = "";
      this.searchButtonShrink = "";
    }

  }


  /*  When user type keywords in search input, searchPopupOnChange will be called.
   *  searchemit in root component will be activated here and will return the value to term.
   *  if term empty then hidemenu eventemitter in root component  will be activated, and if not
   *  then showmenu in root component which will be activated*/
  @Output() showMenu = new EventEmitter();
  @Output() hideMenu = new EventEmitter();
  @Output() searchemit: EventEmitter<string> = new EventEmitter<string>();
  searchPopupOnChange(){
    this.searchemit.emit(this.term);
    if (this.term===''){
      this.hidemenu();
    }else {
      this.showmenu()
    }
  }
  showmenu(){
    this.showMenu.emit(null);
  }

  hidemenu(){
    this.hideMenu.emit(null);
  }

  /* Called from  : This component
   * Function     : Check session storage, if null show LoginPopup, if not go to sellerarea*/
  showloginPopUp(){
    if (this.token&&sessionStorage.getItem("role")==="1") {
      if(!this.jwtHelper.isTokenExpired(this.token)){
        this.router.navigate(['sellerarea']);
      }else {
        this.setVisibleLoginPopup(true);
      }
    }else if(this.token&&sessionStorage.getItem("role")==="2"){
      if(!this.jwtHelper.isTokenExpired(this.token)){
        this.router.navigate(['dashboard']);
      }else {
        this.setVisibleLoginPopup(true);
      }
    }else {
      this.setVisibleLoginPopup(true);
    }
  }


  /* Called from  : this component & login component
   * Function     : to hide login component popup
   *  */
  setVisibleLoginPopup(dis){
    this.visibleLoginPopup=dis;
  }


  /* Called from  : this.ngOnInit
   * Function     : Set button Login Text and Color Base on logged in or not
   * */
  setLoginOrDashboard(){
    if (this.token&&sessionStorage.getItem("role")==="1") {
      if(!this.jwtHelper.isTokenExpired(this.token)){
        this.btnLoginText="Seller Area";
        this.btnLoginColor="#336699";
      }else {
        this.btnLoginText="LOGIN";
        this.btnLoginColor="#ff030c";
      }
    }else if(this.token&&sessionStorage.getItem("role")==="2"){
      if(!this.jwtHelper.isTokenExpired(this.token)){
        this.btnLoginText="Dashboard";
        this.btnLoginColor="#336699";
      }else {
        this.btnLoginText="LOGIN";
        this.btnLoginColor="#ff030c";
      }
    }else {
      this.btnLoginText="LOGIN";
      this.btnLoginColor="#ff030c";
    }
  }
}
