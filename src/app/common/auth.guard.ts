import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {tokenNotExpired, JwtHelper} from 'angular2-jwt';



@Injectable()
export class AuthGuardSeller implements CanActivate {
  token =sessionStorage.getItem("token");
  jwtHelper: JwtHelper = new JwtHelper();
  constructor(private router: Router) {}

  canActivate() {
    if (this.token&&sessionStorage.getItem("role")==="1") {
      return !this.jwtHelper.isTokenExpired(this.token);
    }
    this.router.navigate(['/home']);
    return false;
  }
}


@Injectable()
export class AuthGuardDashboard implements CanActivate {
  token =sessionStorage.getItem("token");
  jwtHelper: JwtHelper = new JwtHelper();
  constructor(private router: Router) {}

  canActivate() {
    if (this.token&&sessionStorage.getItem("role")==="2") {
      return !this.jwtHelper.isTokenExpired(this.token);;
    }
    this.router.navigate(['/home']);
    return false;
  }
}
