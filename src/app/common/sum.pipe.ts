import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sum'
})
export class SumPipe implements PipeTransform {
  loading:any;
  transform(service: any[], term: any): any {
    if (!service || !service.length) {
      return [];
    }else {

    }
    if (term === undefined) return null;
    return service.filter(service => service.product_name.toLowerCase().includes(term.toLowerCase()))
  }
}
