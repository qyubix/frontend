import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from "@angular/router";
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { RatingModule } from 'ngx-bootstrap/rating';



import {
  OverlayPanelModule,
  TabViewModule,
  PanelModule,
  ButtonModule,
  SplitButtonModule,
  SliderModule,
  DataTableModule,
  DialogModule,
  InputTextModule,
  PasswordModule,
  PaginatorModule,
  DropdownModule,
  MultiSelectModule,
  SpinnerModule,
} from 'primeng/primeng';


import { AppComponent } from './app.component';
import { RootComponent } from './component/root/root.component';
import { HeaderComponent } from './component/header/header.component';
import { HomeComponent } from './component/home/home.component';
import {LoginComponent} from "./component/login/login.component";
import {SidecategoryComponent} from "./component/sidecategory/sidecategory.component";
import {SellerareaComponent} from "./component/sellerarea/sellerarea.component";
import {BuyerareaComponent} from "./component/buyerarea/buyerarea.component";
import {SearchpopupComponent} from "./component/searchpopup/searchpopup.component";
import {SeemoreComponent} from "./component/seemore/seemore.component";
import {SearchresultComponent} from "./component/searchresult/searchresult.component";
import {DetailprodComponent} from "./component/detailprod/detailprod.component";
import {DatatableComponent} from "./component/sellerarea/datatable/datatable.component";
import {EmailComponent} from "./component/sellerarea/email/email.component";
import {ProductinfoComponent} from "./component/sellerarea/productinfo/productinfo.component";


import {AuthGuardSeller, AuthGuardDashboard} from "./common/auth.guard";
import {ProductService} from "./service/product.service";
import {UserService} from "./service/user.service";
import {SearchPipe} from "./common/search.pipe";
import {SumPipe} from "./common/sum.pipe";
import {DetailprodService} from "./service/detailprod.service";
import {TrackScrollDirective} from "./component/header/trackscroll.directive";
import {CommonData} from "./common/common.data";
import {LoginService} from "./service/login.service";
import {HttpServiceHandler} from "./service/http.servicehandle";
import {TransactionService} from "./service/transaction.service";
import {ChartData} from "./component/sellerarea/productinfo/chartdata";
import { ReportComponent } from './component/sellerarea/report/report.component';
import {SellerareaheaderComponent} from "./component/sellerarea/sellerareaheader/sellerareaheader.component";
import {CapitalizePipe} from "./common/capitalizefirstletter.pipe";
import {ChartDataBar} from "./component/sellerarea/productinfo/chartdatabar";

const APP_ROUTES:Routes = [
  { path: '', component: RootComponent},
  { path: 'home',component:RootComponent},
  { path: 'sellerarea',component:SellerareaComponent, canActivate:[AuthGuardSeller]},
  { path: 'dashboard',component:BuyerareaComponent, canActivate:[AuthGuardDashboard]},
  { path: 'seemore/:id',component:SeemoreComponent },
  { path: 'searchresult/:name',component:SearchresultComponent },
  { path: 'detailprod/:id', component: DetailprodComponent }
 ];

@NgModule({
  declarations: [
    AppComponent,
    RootComponent,
    HeaderComponent,
    HomeComponent,
    CommonData,
    LoginComponent,
    TrackScrollDirective,
    SidecategoryComponent,
    SellerareaheaderComponent,
    SellerareaComponent,
    BuyerareaComponent,
    SearchpopupComponent,
    SeemoreComponent,
    SearchresultComponent,
    DetailprodComponent,
    DatatableComponent,
    EmailComponent,
    ProductinfoComponent,
    SearchPipe,
    SumPipe,
    CapitalizePipe,
    ReportComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ChartsModule,
    RatingModule,
    TabViewModule,
    PanelModule,
    DropdownModule,
    MultiSelectModule,
    PaginatorModule,
    SpinnerModule,
    DialogModule,
    PasswordModule,
    DataTableModule,
    InputTextModule,
    ButtonModule,
    SliderModule,
    SplitButtonModule,
    OverlayPanelModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [
    LoginService,
    HeaderComponent,
    ProductService,
    UserService,
    LoginService,
    TransactionService,
    DetailprodService,
    HttpServiceHandler,
    SeemoreComponent,
    AuthGuardDashboard,
    AuthGuardSeller,
    ChartData,
    ChartDataBar
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
