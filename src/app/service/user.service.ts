import { Injectable }     from '@angular/core';
import 'rxjs/Rx';
import { environment } from "../../environments/environment"
import {HttpServiceHandler} from "./http.servicehandle";

@Injectable()
export class UserService {

  constructor (private httpServiceHandler:HttpServiceHandler) {}

  getAllUser(limit:number){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/user?limit='+limit
    );
  }

  getUserById(id:string){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/user/'+id
    );
  }
    //
    // getUser (){
    //     return this.http.get(this.userUrl,{headers: this.headers1})
    //         .map(res => <any[]> res.json())
    //         .catch(this.handleError);
    // }
    //
    // getUserWithSize (size){
    //   return this.http.get(this.userUrl+"?size="+size,{headers: this.headers1})
    //     .map(res => <any[]> res.json())
    //     .catch(this.handleError);
    // }
    // getUserById(id){
    //   return this.http.get(this.userUrl+"/"+id,{headers: this.headers1})
    //     .map(res => <any[]> res.json())
    //     .catch(this.handleError);
    // }


}
