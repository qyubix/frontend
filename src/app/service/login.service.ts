import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/Rx';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import {environment} from "../../environments/environment";
import {HeaderComponent} from "../component/header/header.component";
import { Md5 } from 'ts-md5/dist/md5';

@Injectable()
export class LoginService {
  private server= environment.dataserve;
  private key:any;

  constructor(public router: Router,
              private http: Http,
              private headerComponent: HeaderComponent) {

  }

  public logout(){
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('key');
    this.router.navigate(['home']);
    alert("Anda Telah Logout");
  }

  public login(user_name, user_password, role) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    this.key = Md5.hashStr(user_name + user_password);

    if(role==1){
      let body = JSON.stringify({'userName':user_name, 'userPassword':user_password});
      this.http.post(this.server + '/login', body, {headers: headers})
        .subscribe(
          response => {
            console.log(response)
            sessionStorage.setItem('token', response.json().token);
            sessionStorage.setItem('key', response.json().key);
            sessionStorage.setItem('role', response.json().role);
            this.router.navigate(['sellerarea']);
            window.location.reload();
          },
          error => {
            alert(error.text());
            console.log(error.text());
          }
        );
    }else{
      let body = JSON.stringify({'buyerName':user_name, 'buyerPassword':user_password});
      this.http.post(this.server + '/dashboardlogin', body, {headers: headers})
        .subscribe(
          response => {
            console.log(response)
            sessionStorage.setItem('token', response.json().token);
            sessionStorage.setItem('key', response.json().key);
            sessionStorage.setItem('role', response.json().role);
            this.router.navigate(['dashboard']);
            window.location.reload();
          },
          error => {
            alert(error.text());
            console.log(error.text());
          }
        );
    }
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
