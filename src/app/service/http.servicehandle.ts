import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class HttpServiceHandler {
  private headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer '+sessionStorage.getItem("token")
  });

  constructor (private http: Http) {
  }

  httpGet(url){
    return this.http.get(url,{headers: this.headers})
      .map(res => <any[]> res.json())
      .catch(this.handleError);
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
