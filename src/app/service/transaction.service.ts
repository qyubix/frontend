import { Injectable }     from '@angular/core';
import 'rxjs/Rx';
import { environment } from "../../environments/environment"
import {HttpServiceHandler} from "./http.servicehandle";

@Injectable()
export class TransactionService {
  constructor (private httpServiceHandler:HttpServiceHandler) {}

  getTransactionByUserId (userId:string,limit:number){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/transaction?userId='+userId+'&limit='+limit
    );
  }

  getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatus
    (userId:string,transactionStatus:string,limit:number){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/transaction?userId='+userId+'&transactionStatus='+transactionStatus+'&limit='+limit
    );
  }

  getTransactionWithAllRelationshipIdByUserId(userId){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/transactionGetAllRelationship?userId='+userId
    );
  }

  getValuableUser(limit){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/user/valuableuser?limit='+limit
    );
  }

  getTransactionAndProductAndStoreKeyByUserIdAndTransactionStatusWithPagination
    (userId:string,transactionStatus:string,size:number, number:number, limit:number){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/transaction?' +
        'userId='+userId+
        '&size='+size+
        '&number='+number+
        '&transactionStatus='+transactionStatus+
        '&limit='+limit
    );
  }
  //
  // getTransaction (){
  //   return this.http.get(this.transactionUrl,{headers: this.headers1})
  //     .map(res => <any[]> res.json())
  //     .catch(this.handleError);
  // }
  //
  // getTransactionByStatus(status){
  //   return this.http.get(this.transactionUrl+"?transaction_status="+status,{headers: this.headers1})
  //     .map(res => <any[]> res.json())
  //     .catch(this.handleError);
  // }
  //
  // private extractData(res: Response) {
  //   let body = res.json();
  //   return body.data || { };
  // }
  //
  // private handleError (error: any) {
  //   // In a real world app, we might use a remote logging infrastructure
  //   // We'd also dig deeper into the error to get a better message
  //   let errMsg = (error.message) ? error.message :
  //     error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  //   console.error(errMsg); // log to console instead
  //   return Observable.throw(errMsg);
  // }
}
