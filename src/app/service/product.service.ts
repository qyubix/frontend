import { Injectable }     from '@angular/core';
import 'rxjs/Rx';
import { environment } from "../../environments/environment"
import {HttpServiceHandler} from "./http.servicehandle";
@Injectable()
export class ProductService {
  constructor (private httpServiceHandler:HttpServiceHandler) {}

  getProductDetailById(id){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/product/'+id
    );
  }
  getProductSearch(limit:number){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/productsearch?limit='+limit
    );
  }

  getProductByCategory (productCategory:string,limit:number){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/product?productCategory='+productCategory+'&limit='+limit
    );
  }

  getProductByCategoryWithPagination (productCategory:string,size:number,number:number,limit:number){
    return this.httpServiceHandler.httpGet(
      environment.dataserve+'/product?' +
      'productCategory='+productCategory+
      '&size='+size+
      '&number='+number+
      '&limit='+limit
    );
  }

}
