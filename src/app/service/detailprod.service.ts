import { Injectable }     from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import { environment } from "../../environments/environment"
import 'rxjs/Rx';
import {DetailprodComponent} from '../component/detailprod/detailprod.component';
import { ActivatedRoute,Router } from '@angular/router';

@Injectable()
export class DetailprodService {
    error:any;
    date:Date;
    response:any[];
    private id:string;
    private headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+
      sessionStorage.getItem("token")});

  constructor (private http: Http, private route: ActivatedRoute) {

    }


    getDetail(id){
        return this.http.get(environment.dataserve+'/product/'+id)
                    .map(res=>  res.json().content[0]);
    }

    buy(quantity,id){
      this.date= new Date();
      let newTransaction=JSON.stringify({
        'transaction_buyer_id':sessionStorage.getItem("key"),
        'transaction_product_id':id,
        'transaction_quantity':quantity,
        'transaction_status': 'booked'
      });
      this.http.post(environment.dataserve+'/transaction',newTransaction,{headers:this.headers}).subscribe(
        response => {
          alert("Item telah ditambahkan ke keranjang")
        },
        err => {
          alert("Anda tidak dapat booking jika belum login");
          console.log(err);
        }
      )
    }

  private handleError (error: any) {
      alert(error)
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}
